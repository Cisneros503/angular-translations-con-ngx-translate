import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private translate: TranslateService){
    //cambiar esta variable para setear el idioma
    translate.setDefaultLang('en'); 
  }


  title = 'traducciones';
}
